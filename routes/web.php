<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/users/list', [\App\Http\Controllers\MyController::class, 'showUsers']);
Route::get('/users/create/{name}{email}{password}', [\App\Http\Controllers\MyController::class, 'createUser']);
Route::get('/users/single/{id}', [\App\Http\Controllers\MyController::class, 'findUser']);
Route::get('/users/delete/{id}', [\App\Http\Controllers\MyController::class, 'deleteUser']);

