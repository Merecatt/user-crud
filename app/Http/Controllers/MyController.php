<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\User;

class MyController extends Controller
{
    public function showUsers(){
        $users = User::all();

        foreach($users as $user){
            echo $user;
        }
    }

    public function createUser(Request $request){
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->save();
        echo "Created a user $user->name";
        //return redirect('/');
    }

    public function findUser(Request $request){
        $id = $request->id;
        $user = User::findOrFail($id);
        echo $user;
        //return $user;
    }

    public function deleteUser(Request $request){
        $id = $request->id;
        $user = User::findOrFail($id);
        $name = $user->name;
        $user->delete();
        echo "User $name deleted";
    }
}
