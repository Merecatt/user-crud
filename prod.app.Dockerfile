FROM php:7.4-fpm

# # Arguments defined in docker-compose.yml
# ARG user
# ARG uid


# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    wget \
    unzip \
    build-essential \
    libssl-dev \
    libxrender-dev \
    git-core \ 
    libx11-dev \ 
    libxext-dev \
    libfontconfig \ 
    libfreetype6-dev \
    xfonts-75dpi \
    xfonts-base \
    fontconfig \
    libpng16-16 \
    libxrender1 \
    xvfb \
    # multiarch-support \/
    libwebp-dev \
    libjpeg62-turbo-dev \
    libpng-dev libxpm-dev \
    mariadb-client

RUN wget http://security.ubuntu.com/ubuntu/pool/main/libj/libjpeg-turbo/libjpeg-turbo8_1.4.2-0ubuntu3.4_amd64.deb
RUN dpkg --ignore-depends=multiarch-support -i libjpeg-turbo8_1.4.2-0ubuntu3.4_amd64.deb
RUN rm libjpeg-turbo8_1.4.2-0ubuntu3.4_amd64.deb

# # install wktohtml
# RUN wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.bionic_amd64.deb
# RUN dpkg --ignore-depends=multiarch-support -i wkhtmltox_0.12.6-1.bionic_amd64.deb
# RUN rm wkhtmltox_0.12.6-1.bionic_amd64.deb
# RUN ln -s /usr/local/bin/wkhtmltopdf /usr/bin/wkhtmltopdf

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath sockets

RUN docker-php-ext-configure gd \
    --with-webp=/usr/include/ \
    --with-jpeg=/usr/include/ \
    # --with-png=/usr/include/ \
    # --with-zlib=/usr/include/ \
    --with-xpm=/usr/include/ \
    --with-freetype=/usr/include/

RUN docker-php-ext-install gd

# Install PHP redis
RUN pecl install -o -f redis \
&&  rm -rf /tmp/pear \
&&  docker-php-ext-enable redis

# RUN PHP_MEMORY_LIMIT=3GB
# RUN PHP_POST_MAX_SIZE=20M
# RUN PHP_MAX_EXECUTION_TIME=900
# RUN PHP_UPLOAD_MAX_FILEFIZE=20M 
# Memory Limit
RUN echo "memory_limit=\${PHP_MEMORY_LIMIT}" > $PHP_INI_DIR/conf.d/memory-limit.ini
RUN echo "max_execution_time=\${PHP_MAX_EXECUTION_TIME}" >> $PHP_INI_DIR/conf.d/memory-limit.ini
RUN echo "post_max_size=\${PHP_POST_MAX_SIZE}" >> $PHP_INI_DIR/conf.d/memory-limit.ini
RUN echo "upload_max_filesize=\${PHP_UPLOAD_MAX_FILESIZE}" >> $PHP_INI_DIR/conf.d/memory-limit.ini

# Time Zone
# RUN echo "date.timezone=${PHP_TIMEZONE:-UTC}" > $PHP_INI_DIR/conf.d/date_timezone.ini

# Display errors in stderr
RUN echo "display_errors=stderr" > $PHP_INI_DIR/conf.d/display-errors.ini

# Disable PathInfo
RUN echo "cgi.fix_pathinfo=0" > $PHP_INI_DIR/conf.d/path-info.ini

# Disable expose PHP
RUN echo "expose_php=0" > $PHP_INI_DIR/conf.d/path-info.ini


# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Create system user to run Composer and Artisan Commands
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Set working directory
WORKDIR /var/www

COPY . /var/www

# COPY --chown=www:www . /var/www

RUN chown -R www:www /var/www

USER www

RUN touch storage/logs/laravel.log

RUN chmod -R 775 /var/www/storage

RUN php -d memory_limit=-1 /usr/bin/composer install --no-scripts --no-interaction --prefer-dist --optimize-autoloader

EXPOSE 9000
CMD ["php-fpm"]